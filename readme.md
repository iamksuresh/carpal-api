
# Carpal Fleet - API (Node js + Express js)

  - Api to fetch list of restaurants
  - Api to add,remove restaurant to favourite's list

## Installation Steps

  - in the root ddirectory , clone the project - `git clone git@bitbucket.org:iamksuresh/carpal-api.git`
  - In the terminal , execute the following
    ```sh
    - cd <root directory>
    - npm install
    - npm start  
 
### Note : UI will run on Port - 5000


### To change Port -
Edit the Port number in the following files

- https://bitbucket.org/iamksuresh/carpal-api/src/af2410d8a8bc9b80773cf0b03800ceec7c352f99/index.js#lines-4

- https://bitbucket.org/iamksuresh/carpal-ui/src/6fea266b3f421b1138000ae0fa047e98e9bbff77/package.json#lines-5
    